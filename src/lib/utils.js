export default {
  minifyHTML: function(string) {
    let tempResult = string.replace(/(\r\n|\n|\r)/gm,"");
    return  tempResult.replace(/>( *|\t*)</g, '><');
  },
  processProperties: function(propertiesText) {
    let properties = [];

    let lines = propertiesText.split('\n');

    for(let i=0 ; i<lines.length ; i++) {
      if(lines[i] != '' || lines[i][0] != '#') {
        
        let propertyName = lines[i].substring(0,lines[i].indexOf('='));
        const propertyContent = lines[i].substring(lines[i].indexOf('=')+1);

        propertyName = propertyName.replace(" ", "");

        if(propertyName != '') {
          properties.push({
            cle: propertyName,
            contenu: propertyContent
          });
        }
      }
    }

    return properties;
  },
  exportProperties: function(properties) {
    let propertiesText = '';

    properties.forEach(property => {
      let propertyName = property.cle.trim();
      let propertyContent = this.minifyHTML(property.contenu);
      propertiesText += propertyName + '=' + propertyContent + '\n';
    });
    return propertiesText;
  }
}