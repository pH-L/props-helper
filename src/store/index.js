import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    config: {
      nbReso: 0,
    }
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
